# `basta`

`basta` is a collection of header-only C++ utility libraries.

# Design Goals

1. Minimal overhead.
1. Minimal dependency.
1. Balance between optimization and utility.

# Current List of Libraries

| Library | Status | Version | Notes |
| --- | --- | --- | --- |
| hive | wishlist | 0.0 | |
| B++ tree | wishlist | 0.0 | |
| argparse | wishlist | 0.0 | |
| ant colony optimization | wishlist | 0.0 | |
| string manipulation | wishlist | 0.0 | |
| FS util | wishlist | 0.0 | |
| buffer ring | wishlist | 0.0 |  |

# TODO

| Task | Priority | Status | Due |
| --- | --- | --- | --- |
| Find a license | high | open | - |

